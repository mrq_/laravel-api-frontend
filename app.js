'use strict';

define(['resources/assets/scripts/routeResolver'], function () {
    var app = angular.module('LaraApp', ['ngStorage', 'ngRoute', 'angular-loading-bar', 'routeResolverServices']);

    app.constant('urls', {
        BASE: 'http://firstrest.local/#/',
        BASE_API: 'http://spaza.dev/api/'
    })

    app.config(['$routeProvider', 'routeResolverProvider', '$controllerProvider', '$compileProvider', '$filterProvider',
                '$provide', '$httpProvider',
                function ($routeProvider, routeResolverProvider, $controllerProvider, $compileProvider, $filterProvider,
                          $provide, $httpProvider) {
        //Change default views and controllers directory using the following:
        routeResolverProvider.routeConfig.setBaseDirectories('/resources/views/', '/resources/scripts/controllers/');

        app.register = {
            controller: $controllerProvider.register,
            directive: $compileProvider.directive,
            filter: $filterProvider.register,
            factory: $provide.factory,
            service: $provide.service
        };

        //Define routes - controllers will be loaded dynamically
        var route = routeResolverProvider.route;
        $routeProvider
        //route.resolve() now accepts the convention to use (name of controller & view) as well as the
        //path where the controller or view lives in the controllers or views folder if it's in a sub folder.
        //For example, the controllers for customers live in controllers/customers and the views are in views/customers.
        //The controllers for orders live in controllers/orders and the views are in views/orders
        //The second parameter allows for putting related controllers/views into subfolders to better organize large projects
        //Thanks to Ton Yeung for the idea and contribution
        .when('/', route.resolve('Home', 'home/', 'vm'))
        .when('/user/me', route.resolve('Me', 'home/', 'vm'))
        .when('/auth/login', route.resolve('Login', 'auth/login/', 'vm'))
        .otherwise({ redirectTo: '/' });
    }]);

    app.run(['$rootScope', '$location', 'Auth', function ($rootScope, $location, Auth) {
        //Client-side security. Server-side framework MUST add it's
        //own security as well since client-based security is easily hacked
        $rootScope.$on("$routeChangeStart", function (event, next, current) {
            if (next && next.$$route && next.$$route.secure) {
                if (!Auth.user.isAuthenticated) {
                    $rootScope.$evalAsync(function () {
                        Auth.redirectToLogin();
                    });
                }
            }
        });
    }]);

    return app;
});