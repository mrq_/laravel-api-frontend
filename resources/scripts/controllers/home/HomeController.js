
'use strict';

define(['app'], function (app) {
   var injectParams = ['$scope', '$location', '$http', 'urls'];
   var HomeController = function ($scope, $location, $http, urls) {
      console.group("------------ HomeController------------")
      var vm = this;

      vm.todos = [];

      vm.message = 'Everyone come and see how good I am!';
      $http.get(urls.BASE_API + 'todo').success(function(response) {
         $scope.todoList = response.data;
      });
   };

   HomeController.$inject = injectParams;
   app.register.controller('HomeController', HomeController);

   console.groupEnd();
});