
'use strict';

define(['app'], function (app) {
   var injectParams = ['$scope', '$location', '$http', 'urls'];
   var MeController = function ($scope, $location, $http, urls) {
      console.group("------------ Me Controller------------")
      var vm = this;

      vm.m = [];

      $http.get(urls.BASE_API + 'users/me').success(function(response) {
         $scope.user = response.data;
      });
   };

   MeController.$inject = injectParams;
   app.register.controller('MeController', MeController);

   console.groupEnd();
});