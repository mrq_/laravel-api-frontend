'use strict';

define(['app'], function (app) {
    var injectParams = ['$scope', '$location', '$localStorage', '$rootScope', 'Auth', 'urls'];
    var LoginController = function ($scope, $location, $localStorage, $rootScope, Auth, urls) {
        console.group("------------ Login Controller------------");

        function successAuth (result) {
            $localStorage.token = result.token;
            window.location = urls.BASE + "user/me";
        };

        var vm = $scope;
        vm.message = 'Look! I am a Login page.';
        vm.tokenClaims = Auth.tokenClaims();
        vm.email = null;
        vm.password = null;
        vm.errorMessage = null;
        // console.table(vm.tokenClaims);

        vm.login = function () {
            var formData = {
                email: vm.email,
                password: vm.password
            };

            Auth.login(formData, successAuth, function () {
                $rootScope.error = 'Invalid credentials.';
            });

            vm.token = $localStorage.token;
        };
    };

    LoginController.$inject = injectParams;
    app.register.controller('LoginController', LoginController);

    console.groupEnd();
});