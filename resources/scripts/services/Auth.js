'use strict';

define(['app'], function (app) {
    var injectParams = ['$http', '$rootScope', '$location', '$localStorage', 'urls'];
    var authFactory = function ($http, $rootScope, $location, $localStorage, urls) {
        var factory = {
            path: {
                'home': '/',
                'login': urls.BASE_API + 'login',
                'logout': urls.BASE_API + 'logout'
            },
            urlBase64Decode: function (str) {
                var output = str.replace('-', '+').replace('_', '/');
                switch (output.length % 4) {
                    case 0:
                        break;
                    case 2:
                        output += '==';
                        break;
                    case 3:
                        output += '=';
                        break;
                    default:
                        throw 'Illegal base64url string!';
                }
                return window.atob(output);
            }
        };

        factory.tokenClaims = function () {
            var token = $localStorage.token;
            var user = {};
            if (typeof token !== 'undefined') {
                var encoded = token.split('.')[1];
                user = JSON.parse(factory.urlBase64Decode(encoded));
            }
            return user;
        };

        factory.login = function (data, success, error) {
            return $http.post(factory.path['login'], data).success(success).error(error);
        };

        factory.logout = function () {
            return $http.post(factory.path['logout']).then(
                function (results) {
            });
        };

        factory.redirectToLogin = function () {
            $rootScope.$broadcast('redirectToLogin', null);
        };

        return factory;
    };

    authFactory.$inject = injectParams;
    app.factory('Auth', authFactory);
});