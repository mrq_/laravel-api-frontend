'use strict';

define(['app'], function (app) {
    app.config(['$httpProvider', function ($httpProvider) {

        var injectParams  = ['$q', '$rootScope', '$location', '$localStorage'];
        var interceptHttp = function ($q, $rootScope, $location, $localStorage) {
            return {
                'request': function (config) {
                    config.headers = config.headers || {};
                    if ($localStorage.token) {
                        config.headers.Authorization = 'Bearer ' + $localStorage.token;
                    }
                    return config;
                },
                'responseError': function (response) {
                    if (response.status === 400 || response.status === 401 || response.status === 403) {
                        $location.path('/auth/login');
                    }
                    return $q.reject(response);
                }
            };
        };

        interceptHttp.$inject = injectParams;
        $httpProvider.interceptors.push(interceptHttp);
    }]);

});