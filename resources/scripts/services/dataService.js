'use strict';

define(['app'], function (app) {

    var injectParams = ['config', 'todoService'];

    var dataService = function (config, todoService) {
        return todoService;
    };

    dataService.$inject = injectParams;

    app.factory('dataService', ['config', 'todoService', dataService]);

});