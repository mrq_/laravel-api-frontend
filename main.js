require.config({
    baseUrl: 'http://firstrest.local',
    // urlArgs: 'v=1.0'
});

require(
    [
        'app',
        'resources/assets/scripts/routeResolver',
        'resources/scripts/services/Auth',
        'resources/scripts/services/httpInterceptors',
        // 'resources/scripts/services/config',
        // 'resources/scripts/services/dataService',
    ],
    function () {
        angular.bootstrap(document, ['LaraApp']);
    }
);